# golang:1.16.2-alpine3.13
FROM golang@sha256:12d5f94cd4d2840e538e82e26a5dfddf711b30cc98a9f6e01bcf65d7aaf7ccd8 as builder
WORKDIR /app

COPY go.mod go.sum /app/
RUN go mod download

ENV CGO_ENABLED=0
COPY . /app/
RUN go build -o /go/bin/requestertwilio ./cmd/requestertwilio

# The following builds from scratch an image with only the binary file.
FROM scratch
LABEL maintainer="sbernier@ravnmsg.io"
WORKDIR /app

COPY --from=builder /go/bin/requestertwilio /app/
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

USER 10001:10001

ENTRYPOINT ["./requestertwilio"]
