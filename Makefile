# Builds the development dockerfile and restart the docker-compose service if up
.PHONY: build
build:
	@if [[ -n "$(shell docker-compose top | grep requester-twilio)" ]]; then \
		docker-compose up --detach --build requester-twilio; \
	else \
		docker-compose build; \
	fi

# Run code style tests
.PHONY: test-lint
test-lint:
	@docker run \
		--workdir /app \
		--volume ${PWD}:/app \
		--rm \
		golangci/golangci-lint:latest-alpine \
		golangci-lint run

# Run tests
.PHONY: test
test:
	go test ${TEST_ARGS} -race ./...

.DEFAULT_GOAL := build
