# Ravn Twilio Requester

The Twilio Requester service delivers a message by SMS using [Twilio](https://www.twilio.com/).

## Configuration

A Twilio account SID and auth token must be specified using the `RAVN_REQUESTER_TWILIO_ACCOUNT_SID` and `RAVN_REQUESTER_TWILIO_AUTH_TOKEN` environment variables, respectively.

By default, the service will listen to a random open port, but this can be configured with the `RAVN_REQUESTER_TWILIO_SERVER_PORT` environment variable.

The service's latest Docker image can be pulled from `registry.gitlab.com/ravnmsg/ravn-requester-twilio:latest`. The following command will retrieve and start the service on port `50102`:

```bash
docker run --publish 50102:50102 --env RAVN_REQUESTER_TWILIO_SERVER_PORT=50102 --env RAVN_REQUESTER_TWILIO_ACCOUNT_SID=(account sid) RAVN_REQUESTER_TWILIO_AUTH_TOKEN=(auth token) --rm registry.gitlab.com/ravnmsg/ravn-requester-twilio:latest
```

## Usage

The service listens for POST requests on the `/request` path. The request body must be an `application/json` with the following keys:

* `from`, the source phone number in E.164 format
* `to`, the target phoone number in E.164 format
* `content`, the SMS body

```bash
$ curl -XPOST localhost:50102/request --data '{"from":"+18005551000","to":"+18005551001","content":"SMS content."}' | jq
{
    "externalId": "SMca69d78017be4889aaf3ba83e6941982"
}
```

## Adding to Ravn

This service will need to be configured as a delivery kind in Ravn. The following resources are required:

* A delivery kind with `host` set to the Twilio Requester service's host, eg. `localhost:50102`
* One template kind with `target_attr` set to `content`
* One template for each combination of message kind and language tag for which a Twilio SMS should be deliverable

The template body for `content` will be generated as the SMS's body.

Template bodies are generated using [Go templates](https://golang.org/pkg/text/template/). All input provided in the initial request will be available in the template body.

Ravn can then receive requests to deliver messages using Twilio:

```bash
$ curl -XPOST https://api.ravnmsg.io/v1/namespaces/default/messages --data '{"messageKind":"password_reset","languageTag":"en","deliveryKind":"twilio","input":{"username":"John"}}' | jq
{
  "data": {
    "id": "2636b571-2b7e-4d51-bc9b-492cd6e74307",
    "messageKind": "password_reset",
    "languageTag": "en",
    "input": {
      "username": "John"
    },
    "status": "pending",
    "createTime": "2021-03-19T14:19:55.987426Z",
    "deliveries": []
  },
  "meta": {
    "requestId": "80360336-8afb-448e-ad01-163588418ba5"
  }
}
```
