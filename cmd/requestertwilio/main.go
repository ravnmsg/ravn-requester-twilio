package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"gitlab.com/ravnmsg/ravn-requester-twilio/internal/requester/twiliorequester"
	"gitlab.com/ravnmsg/ravn-requester/pkg/app"
	"gitlab.com/ravnmsg/ravn-requester/pkg/server/httpserver"
)

func main() {
	srv, err := httpserver.New(os.Getenv("RAVN_REQUESTER_TWILIO_SERVER_PORT"))
	if err != nil {
		log.Printf("Error creating HTTP server: %s", err)
		os.Exit(1)
	}

	requester, err := newTwilioRequester()
	if err != nil {
		log.Printf("Error configuring requester: %s", err)
		os.Exit(1)
	}

	if err := app.Start(context.Background(), srv, requester.RequestFn); err != nil {
		log.Printf("Error starting requester: %s", err)
		os.Exit(1)
	}
}

func newTwilioRequester() (*twiliorequester.Requester, error) {
	accountSID := os.Getenv("RAVN_REQUESTER_TWILIO_ACCOUNT_SID")
	if accountSID == "" {
		return nil, fmt.Errorf("error with RAVN_REQUESTER_TWILIO_ACCOUNT_SID: empty value")
	}

	authToken := os.Getenv("RAVN_REQUESTER_TWILIO_AUTH_TOKEN")
	if authToken == "" {
		return nil, fmt.Errorf("error with RAVN_REQUESTER_TWILIO_AUTH_TOKEN: empty value")
	}

	r, err := twiliorequester.New(accountSID, authToken)
	if err != nil {
		return nil, err
	}

	return r, nil
}
