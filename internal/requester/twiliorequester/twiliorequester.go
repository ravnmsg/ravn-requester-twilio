package twiliorequester

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"gitlab.com/ravnmsg/ravn-requester/pkg/requester"
)

// Requester defines a Requester service whose execution results in a request to Twilio.
type Requester struct {
	accountSID string
	authToken  string
}

// New creates a new Twilio Requester service.
func New(accountSID, authToken string) (*Requester, error) {
	if accountSID == "" {
		return nil, fmt.Errorf("error using Twilio account SID: empty value")
	}

	if authToken == "" {
		return nil, fmt.Errorf("error using Twilio authentication token: empty value")
	}

	r := &Requester{
		accountSID: accountSID,
		authToken:  authToken,
	}

	return r, nil
}

// RequestFn performs a request to the Twilio third-party service.
//
// The following input values are required:
// * from, the source phone number
// * to, the target phone number
// * content, the SMS's content
//
// If successful, the response will contain some values that are going to be used to determine the
// status of the delivery: status, error_code, and error_message. The Twilio message's ID can be
// found in the sid attribute.
func (r *Requester) RequestFn(input *requester.Values) (*requester.Values, error) {
	uri := fmt.Sprintf("https://api.twilio.com/2010-04-01/Accounts/%s/Messages.json", r.accountSID)

	reqBody := r.buildRequestBody(input)
	req, err := http.NewRequest(http.MethodPost, uri, reqBody)
	if err != nil {
		return nil, fmt.Errorf("error building request: %s", err)
	}

	req.SetBasicAuth(r.accountSID, r.authToken)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error performing request: %s", err)
	}

	respBody, err := r.readResponseBody(resp)
	if err != nil {
		return nil, fmt.Errorf("error reading response body: %s", err)
	} else if resp.StatusCode >= 400 {
		return nil, fmt.Errorf("error during request to Twilio: %s", respBody)
	}

	result, err := r.parseResponseBody(respBody)
	if err != nil {
		return nil, fmt.Errorf("error parsing response body: %s", err)
	} else if result.ErrorCode != 0 {
		return nil, fmt.Errorf("error during request to Twilio: [%d] %s", result.ErrorCode, result.ErrorMessage)
	}

	output := &requester.Values{
		"externalId": result.SID,
	}

	return output, nil
}

func (r *Requester) buildRequestBody(input *requester.Values) *strings.Reader {
	data := url.Values{}
	data.Set("Body", input.GetString("content"))
	data.Set("From", input.GetString("from"))
	data.Set("To", input.GetString("to"))

	return strings.NewReader(data.Encode())
}

func (r *Requester) readResponseBody(resp *http.Response) ([]byte, error) {
	defer resp.Body.Close()

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return respBody, nil
}

func (r *Requester) parseResponseBody(respBody []byte) (*twilioResponse, error) {
	var result *twilioResponse
	if err := json.Unmarshal(respBody, &result); err != nil {
		return nil, err
	}

	return result, nil
}

// Because callbacks are not yet implemented in Ravn, a delivery is considered successful if the
// initial request to Twilio succeeds. The SMS can later fail, but Ravn will be be notified.
//
// Until this time, the error_code key is used to determine the delivery status.
//
// ref. https://www.twilio.com/docs/sms/api/message-resource
type twilioResponse struct {
	SID          string `json:"sid"`
	Status       string `json:"status"`
	ErrorCode    int    `json:"error_code"`
	ErrorMessage string `json:"error_message"`
}
